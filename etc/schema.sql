-- Таблица для основных курсов за которыми мы должны следить
CREATE TABLE assets (
    id SMALLSERIAL PRIMARY KEY,
    symbol VARCHAR(20) NOT NULL
);

INSERT INTO assets (symbol) VALUES('EURUSD');
INSERT INTO assets (symbol) VALUES('USDJPY');
INSERT INTO assets (symbol) VALUES('GBPUSD');
INSERT INTO assets (symbol) VALUES('AUDUSD');
INSERT INTO assets (symbol) VALUES('USDCAD');

-- Таблица для истории курсов
CREATE TABLE assets_history (
    id serial PRIMARY KEY,
    asset_id SMALLINT REFERENCES assets (id),
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    val NUMERIC(20, 6) NOT NULL
);

CREATE INDEX assets_history_created_on_and_asset_idx ON assets_history (asset_id, (created_on::DATE));
