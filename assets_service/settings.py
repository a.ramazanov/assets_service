import pathlib
import yaml


BASE_DIR = pathlib.Path(__file__).parent.parent
DEFAULT_CONFIG_PATH = BASE_DIR / "config" / "development.yaml"


def get_config(path: str) -> dict:
    with open(path) as f:
        config = yaml.safe_load(f)
    return config
