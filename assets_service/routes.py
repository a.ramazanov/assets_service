from views import assets, ping
from aiohttp import web


def setup_routes(app: web.Application) -> None:
    app.router.add_get("/assets", assets, name="assets")
    app.router.add_get("/ping", ping, name="ping")
