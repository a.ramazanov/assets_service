import asyncpg
from aiohttp import web


async def init_db(app: web.Application) -> None:
    """
    Инициализация базы и сохранения в db_pool пула соединений к базе
    """
    db_config = app["config"]["database"]
    dsn = get_dsn(db_config)
    pool = await asyncpg.create_pool(
        dsn=dsn, min_size=db_config["min_size"], max_size=db_config["max_size"]
    )
    app["db_pool"] = pool


def get_dsn(config: dict) -> str:
    return (
        f'postgresql://{config["user"]}:{config["password"]}@'
        f'{config["host"]}:{config["port"]}/{config["database"]}'
    )
