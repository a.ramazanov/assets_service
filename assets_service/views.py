import asyncio
import aiohttp
from aiohttp.web_response import Response
from aiohttp.web_ws import WebSocketResponse

from service.tasks import assets_history_task
from service.proto import ProtoInput, ProtoOutput


async def ping(request) -> Response:
    """
    Функция для проверки, что сервис живой
    """
    return aiohttp.web.Response(text="OK")


async def assets(request) -> WebSocketResponse:
    """
    Основная функция для взаимодействия с клиентом через websocket
    """
    queries = request.app["queries"]

    ws = aiohttp.web.WebSocketResponse()
    await ws.prepare(request)

    timer_callback = None
    async for msg in ws:
        if timer_callback is not None:
            timer_callback.cancel()
        if msg.type == aiohttp.WSMsgType.TEXT:
            proto_command = ProtoInput(msg.json())
            if proto_command.is_assets_action():
                assets_rows = await queries.get_assets()
                assets = ProtoOutput.construct_assets(assets_rows)
                await ws.send_json(assets)
            elif proto_command.is_subscribe_action():
                asset_id = proto_command.get_asset_id()
                timer_callback = asyncio.get_event_loop().create_task(
                    assets_history_task(ws, queries, asset_id)
                )
            elif msg.data == "close":
                await ws.close()

    return ws
