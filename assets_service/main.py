import logging
import asyncio
from aiohttp import web
from collections import defaultdict

from db import init_db
from routes import setup_routes
from service.tasks import fetch_and_save_assets_task
from settings import get_config, DEFAULT_CONFIG_PATH
from service.queries import Queries


async def on_startup(app: web.Application) -> None:
    """
    Инициализация класса для запросов и запуск
    фоновых задач при старте приложения
    """
    app["queries"] = Queries(app["db_pool"])

    event_loop = asyncio.get_event_loop()
    assets_url = app["config"]["app"]["assets_url"]
    app["tasks"] = defaultdict(dict)
    app["tasks"]["fetch_assets"] = event_loop.create_task(
        fetch_and_save_assets_task(assets_url, app["queries"])
    )


async def on_cleanup(app: web.Application) -> None:
    """
    Завершение фоновых задач и закрытие пула соединений с базой
    """
    app["tasks"]["fetch_assets"].cancel()
    await app["db_pool"].close()


async def init_app(config: dict) -> web.Application:
    """
    Инициализация приложения и базы
    """
    app = web.Application()
    app["config"] = config
    await init_db(app)
    setup_routes(app)

    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    return app


def main(config_path: str = DEFAULT_CONFIG_PATH) -> None:
    """
    Основная функция для старта приложения и использования конфига для нужного окружения
    """
    logging.basicConfig(level=logging.DEBUG)
    config = get_config(config_path)
    app = init_app(config)
    web.run_app(app, host=config["app"]["host"], port=config["app"]["port"])


if __name__ == "__main__":
    main()
