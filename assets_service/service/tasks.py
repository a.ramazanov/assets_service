import aiohttp
import asyncio
import json
import logging


from service.proto import ProtoOutput
from service.queries import Queries
from service.utils import (
    raw_assets_to_json,
    filter_assets,
    prepare_for_assets_history_insert,
)


async def assets_history_task(ws, queries: Queries, asset_id: int) -> None:
    """
    Фоновая задача для селекта истории котировок клиенту из базы
    """
    last_id = None
    while True:
        await asyncio.sleep(1)

        if last_id:
            assets_history_rows = await queries.get_assets_history_from_id(
                asset_id, last_id
            )
        else:
            assets_history_rows = await queries.get_assets_history(asset_id)
        if len(assets_history_rows):
            if last_id:
                assets = ProtoOutput.construct_asset_point(assets_history_rows)
            else:
                assets = ProtoOutput.construct_asset_history(assets_history_rows)

            last_id = assets_history_rows[-1]["id"]
            try:
                await ws.send_str(json.dumps(assets, default=str))
            except ConnectionResetError:
                return


async def fetch_and_save_assets_task(url: str, queries: Queries) -> None:
    """
    Фоновая задача для обновление курсов
    """
    while True:
        await asyncio.sleep(2)

        async with aiohttp.ClientSession() as session:
            logging.getLogger("aiohttp.client").debug(f"Starting request to {url}")
            async with session.get(url) as resp:
                text = await resp.text()
                data = raw_assets_to_json(text)
                assets_rows = await queries.get_assets()
                allowed_assets = {r["symbol"]: r["id"] for r in assets_rows}
                filtered_data = filter_assets(data, allowed_assets)
                insert_data = prepare_for_assets_history_insert(
                    filtered_data, allowed_assets
                )
                await queries.insert_assets_history(insert_data)
