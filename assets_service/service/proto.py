from schema import Schema, Or, Optional
from asyncpg import Record
from typing import List


class ProtoInput:
    """
    Класс для проверки входных сообщений через websocket и определения типа запроса
    """

    action_assets = "assets"
    action_subscribe = "subscribe"
    schema = Schema(
        {
            "action": Or(action_assets, action_subscribe),
            "message": {Optional("assetId"): int},
        }
    )

    def __init__(self, msg):
        self.schema.validate(msg)
        self.action = msg["action"]
        self.message = msg["message"]

    def is_assets_action(self) -> bool:
        return self.action == self.action_assets

    def is_subscribe_action(self) -> bool:
        return self.action == self.action_subscribe

    def get_asset_id(self) -> int:
        return self.message.get("assetId")


class ProtoOutput:
    """
    Класс для формирования сообщений клиенту через websocket
    """

    @staticmethod
    def construct_assets(rows: List[Record]) -> dict:
        res = {
            "action": "assets",
            "message": {"assets": []},
        }
        for r in rows:
            res["message"]["assets"].append({"id": r["id"], "name": r["symbol"]})
        return res

    @staticmethod
    def construct_asset_history(rows: List[Record]) -> dict:
        res = {
            "action": "asset_history",
            "message": {"points": []},
        }

        for r in rows:
            res["message"]["points"].append(
                {
                    "assetId": r["asset_id"],
                    "assetName": r["symbol"],
                    "value": r["val"],
                    "time": r["created_on"],
                }
            )

        return res

    @staticmethod
    def construct_asset_point(rows: List[Record]) -> dict:
        res = {
            "action": "point",
            "message": {},
        }

        for r in rows:
            res["message"] = {
                "assetId": r["asset_id"],
                "assetName": r["symbol"],
                "value": r["val"],
                "time": r["created_on"],
            }

        return res
