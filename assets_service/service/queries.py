from typing import Iterable, List
import logging


class Queries:
    """
    Класс для запросов в БД
    """

    def __init__(self, db_pool):
        self._db_pool = db_pool

    async def get_assets(self):
        async with self._db_pool.acquire() as conn:
            return await conn.fetch("SELECT id, symbol FROM assets ORDER BY id")

    async def get_assets_history(self, asset_id: int):
        async with self._db_pool.acquire() as conn:
            return await conn.fetch(
                "SELECT ah.id, ah.created_on, ah.val, ah.asset_id, a.symbol "
                "FROM assets_history ah "
                "JOIN assets a ON ah.asset_id = a.id "
                "WHERE ah.asset_id = $1 AND date(created_on) IN (current_date, (current_date - INTEGER '1')) "
                "AND ah.created_on >= (NOW() - INTERVAL '30 mins') ORDER BY ah.id",
                asset_id,
            )

    async def get_assets_history_from_id(self, asset_id: int, from_id: int):
        async with self._db_pool.acquire() as conn:
            return await conn.fetch(
                "SELECT ah.id, ah.created_on, ah.val, ah.val, ah.asset_id, a.symbol "
                "FROM assets_history ah "
                "JOIN assets a ON ah.asset_id = a.id "
                "WHERE ah.asset_id = $1 AND ah.id > $2 ORDER BY ah.id LIMIT 1",
                asset_id,
                from_id,
            )

    async def insert_assets_history(self, insert_data: List[Iterable]):
        async with self._db_pool.acquire() as conn:
            logging.getLogger("aiohttp.server").debug(
                f"Insert {len(insert_data)} row(s) into assets_history"
            )
            return await conn.executemany(
                "INSERT INTO assets_history (asset_id, val) VALUES ($1, $2)",
                insert_data,
            )
