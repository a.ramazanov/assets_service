import json


def raw_assets_to_json(text: str) -> list:
    """
    Функция для подготовки данных и подгонки для json
    """
    prepared_string = text.replace("null(", "").replace(");", "").replace("NaN", "null")
    return json.loads(prepared_string)


def filter_assets(data: list, allowed_assets: dict) -> list:
    """
    Функция для фильтрации нужных курсов
    """
    return list(filter(lambda d: d["Symbol"] in allowed_assets.keys(), data["Rates"]))


def prepare_for_assets_history_insert(data: list, allowed_assets: dict) -> list:
    """
    Функция для подготовки строк для обновления истории в БД
    """
    res = []
    for asset in data:
        asset_val = (asset["Bid"] + asset["Ask"]) / 2
        asset_id = allowed_assets[asset["Symbol"]]
        res.append((asset_id, asset_val))

    return res
