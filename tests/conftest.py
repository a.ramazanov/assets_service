import pytest

from main import init_app
from settings import BASE_DIR, get_config

TEST_CONFIG_PATH = BASE_DIR / "config" / "test.yaml"


@pytest.fixture
async def client(aiohttp_client):
    """
    Инициализация приложения с тестовым конфигом
    """
    app = await init_app(get_config(TEST_CONFIG_PATH))
    return await aiohttp_client(app)
