import pytest


async def test_ping(client):
    resp = await client.get("/ping")
    body = await resp.text()
    assert resp.status == 200
    assert body == "OK"


async def test_get_assets(client):
    ws = await client.ws_connect("/assets")
    await ws.send_str('{"action":"assets", "message":{}}')
    msg = await ws.receive()
    assets_data = msg.json()
    await ws.close()
    assert isinstance(assets_data, dict)
    assert assets_data.get("action") == "assets"
    assert isinstance(assets_data.get("message"), dict)
    assert isinstance(assets_data["message"].get("assets"), list)
    assert len(assets_data["message"]["assets"])
    asset = assets_data["message"]["assets"][0]
    assert "id" in asset
    assert "name" in asset
