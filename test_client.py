import asyncio
import os
import json
import aiohttp

HOST = os.getenv("HOST", "127.0.0.1")
PORT = int(os.getenv("PORT", 8080))

URL = f"http://{HOST}:{PORT}/assets"


async def main():
    session = aiohttp.ClientSession()
    async with session.ws_connect(URL) as ws:

        await prompt_and_send(ws)
        async for msg in ws:
            print("Message received from server:", msg)
            # await prompt_and_send(ws)

            if msg.type in (aiohttp.WSMsgType.CLOSED, aiohttp.WSMsgType.ERROR):
                break


async def prompt_and_send(ws):
    new_msg_to_send = input("Type a message to send to the server: ")
    if new_msg_to_send == "exit":
        print("Exiting!")
        raise SystemExit(0)
    elif new_msg_to_send == "assets":
        new_msg_to_send = json.dumps({"action": "assets", "message": {}})
    elif new_msg_to_send.startswith("sub asset"):
        asset_id = new_msg_to_send.split()[2]
        new_msg_to_send = json.dumps(
            {"action": "subscribe", "message": {"assetId": int(asset_id)}}
        )
    await ws.send_str(new_msg_to_send)


if __name__ == "__main__":
    print('Type "exit" to quit')
    asyncio.run(main())
