# Assets service

## Запуск сервиса

Тестировалось на Python 3.10.9. 

Установка зависимостей.

```shell
pip intsall -r etc/requirements.txt
```

Устанавливаем PostgreSQL, создаём БД и применяем схему.

```shell
psql -h 127.0.0.1 NAME < etc/schema.sql
```

Запуск приложения из корня. Конфиг используется по умолчанию config/development.yaml

```shell
python assets_service/main.py
```

## Запуск тестов

Запуск из корня.

```shell
pytest
```

## Тестовый клиент

Запуск из корня.

```shell
python test_client.py
```

Type a message to send to the server:

Список котировок.

```
assets
```

Подписка на котировки c assetId 2

```
sub assets 2
```